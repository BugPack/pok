package com.example.pok

import com.example.pok.model.Card
import com.example.pok.model.CardSuit
import com.example.pok.model.CardValue
import com.example.pok.model.Combination
import org.junit.Assert
import org.junit.Test

class PokerFragmentUnitTest {
    @Test
    fun testCheckCombination() {
        val cardUtils = CardUtils
        var cards = generateCardsCombination(Combination.FLASH_ROYAL)
        Assert.assertEquals(
            "test_CheckFlashRoyal",
            Combination.FLASH_ROYAL,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.STREET_FLASH)
        Assert.assertEquals(
            "test_CheckStreetFlash",
            Combination.STREET_FLASH,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.CARE)
        Assert.assertEquals(
            "test_CheckCare",
            Combination.CARE,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.FULL_HOUSE)
        Assert.assertEquals(
            "test_CheckFullHouse",
            Combination.FULL_HOUSE,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.FLASH)
        Assert.assertEquals(
            "test_CheckFlash",
            Combination.FLASH,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.STREET)
        Assert.assertEquals(
            "test_checkStreet",
            Combination.STREET,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.TRIPLE)
        Assert.assertEquals(
            "test_checkTripe",
            Combination.TRIPLE,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.TWO_PAIRS)
        Assert.assertEquals(
            "test_checkTwoPairs",
            Combination.TWO_PAIRS,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.PAIR)
        Assert.assertEquals(
            "test_checkPair",
            Combination.PAIR,
            cardUtils.checkCombination(cards, arrayListOf())
        )
        cards = generateCardsCombination(Combination.HIGH_CART)
        Assert.assertEquals(
            "test_checkHighCard",
            Combination.HIGH_CART,
            cardUtils.checkCombination(cards, arrayListOf())
        )
    }

    @Test
    fun testCheckFlashRoyal() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.FLASH_ROYAL)
        Assert.assertTrue("test_CheckFlashRoyal fail", cardUtils.checkFlashRoyal(cards))
    }

    @Test
    fun testCheckStreetFlash() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.STREET_FLASH)
        Assert.assertTrue("test_CheckStreetFlash fail", cardUtils.checkStreetFlash(cards))
    }

    @Test
    fun testCheckCare() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.CARE)
        Assert.assertTrue("test_CheckCare fail", cardUtils.checkCare(cards))
    }

    @Test
    fun testCheckFullHouse() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.FULL_HOUSE)
        Assert.assertTrue("test_CheckFullHouse fail", cardUtils.checkFullHouse(cards))
    }

    @Test
    fun testCheckFlash() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.FLASH)
        Assert.assertTrue("test_CheckFlash fail", cardUtils.checkFlash(cards))
    }

    @Test
    fun testCheckStreet() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.STREET)
        Assert.assertTrue("test_CheckStreet fail", cardUtils.checkStreet(cards))
    }

    @Test
    fun testCheckTriple() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.TRIPLE)
        Assert.assertTrue("test_CheckTriple fail", cardUtils.checkTriple(cards))
    }

    @Test
    fun testCheckTwoPairs() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.TWO_PAIRS)
        Assert.assertTrue("test_CheckTwoPairs fail", cardUtils.checkTwoPairs(cards))
    }

    @Test
    fun testCheckPair() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.PAIR)
        Assert.assertTrue("test_CheckPair fail", cardUtils.checkPair(cards))
    }

    @Test
    fun testFindHighCard() {
        val cardUtils = CardUtils
        val cards = generateCardsCombination(Combination.HIGH_CART)
        Assert.assertEquals(
            "test_CheckHighCard fail",
            Card(CardValue._A, CardSuit.Peaks),
            cardUtils.findHighCard(cards)
        )
    }

    private fun generateCardsCombination(combination: Combination): ArrayList<Card> {
        return when (combination) {
            Combination.HIGH_CART -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Hearts),
                Card(CardValue._6, CardSuit.Clubs),
                Card(CardValue._A, CardSuit.Peaks),
                Card(CardValue._10, CardSuit.Diamonds),
                Card(CardValue._Q, CardSuit.Hearts),
                Card(CardValue._8, CardSuit.Clubs)
            )
            Combination.PAIR -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Hearts),
                Card(CardValue._6, CardSuit.Clubs),
                Card(CardValue._8, CardSuit.Peaks),
                Card(CardValue._10, CardSuit.Diamonds),
                Card(CardValue._2, CardSuit.Hearts),
                Card(CardValue._Q, CardSuit.Clubs)
            )
            Combination.TWO_PAIRS -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Hearts),
                Card(CardValue._6, CardSuit.Clubs),
                Card(CardValue._4, CardSuit.Peaks),
                Card(CardValue._10, CardSuit.Diamonds),
                Card(CardValue._2, CardSuit.Hearts),
                Card(CardValue._Q, CardSuit.Clubs)
            )
            Combination.TRIPLE -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Hearts),
                Card(CardValue._2, CardSuit.Clubs),
                Card(CardValue._8, CardSuit.Peaks),
                Card(CardValue._10, CardSuit.Diamonds),
                Card(CardValue._2, CardSuit.Hearts),
                Card(CardValue._A, CardSuit.Clubs)
            )
            Combination.STREET -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._3, CardSuit.Hearts),
                Card(CardValue._4, CardSuit.Clubs),
                Card(CardValue._5, CardSuit.Peaks),
                Card(CardValue._8, CardSuit.Diamonds),
                Card(CardValue._6, CardSuit.Hearts),
                Card(CardValue._A, CardSuit.Clubs)
            )
            Combination.FLASH -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Diamonds),
                Card(CardValue._6, CardSuit.Diamonds),
                Card(CardValue._7, CardSuit.Peaks),
                Card(CardValue._9, CardSuit.Diamonds),
                Card(CardValue._J, CardSuit.Hearts),
                Card(CardValue._A, CardSuit.Diamonds)
            )
            Combination.FULL_HOUSE -> arrayListOf(
                Card(CardValue._2, CardSuit.Diamonds),
                Card(CardValue._A, CardSuit.Hearts),
                Card(CardValue._5, CardSuit.Clubs),
                Card(CardValue._2, CardSuit.Peaks),
                Card(CardValue._9, CardSuit.Diamonds),
                Card(CardValue._A, CardSuit.Diamonds),
                Card(CardValue._2, CardSuit.Hearts)
            )
            Combination.CARE -> arrayListOf(
                Card(CardValue._Q, CardSuit.Diamonds),
                Card(CardValue._4, CardSuit.Hearts),
                Card(CardValue._Q, CardSuit.Clubs),
                Card(CardValue._A, CardSuit.Peaks),
                Card(CardValue._10, CardSuit.Diamonds),
                Card(CardValue._Q, CardSuit.Peaks),
                Card(CardValue._Q, CardSuit.Hearts)
            )
            Combination.STREET_FLASH -> arrayListOf(
                Card(CardValue._3, CardSuit.Diamonds),
                Card(CardValue._6, CardSuit.Diamonds),
                Card(CardValue._Q, CardSuit.Clubs),
                Card(CardValue._5, CardSuit.Diamonds),
                Card(CardValue._10, CardSuit.Hearts),
                Card(CardValue._4, CardSuit.Diamonds),
                Card(CardValue._7, CardSuit.Diamonds)
            )
            Combination.FLASH_ROYAL -> arrayListOf(
                Card(CardValue._A, CardSuit.Clubs),
                Card(CardValue._10, CardSuit.Clubs),
                Card(CardValue._Q, CardSuit.Clubs),
                Card(CardValue._5, CardSuit.Peaks),
                Card(CardValue._K, CardSuit.Clubs),
                Card(CardValue._4, CardSuit.Diamonds),
                Card(CardValue._J, CardSuit.Clubs)
            )
        }
    }
}