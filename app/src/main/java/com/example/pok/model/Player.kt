package com.example.pok.model

enum class Player {
    MY,
    FIRST,
    SECOND,
    THIRD,
    FOUR,
    FIVE
}