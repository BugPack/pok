package com.example.pok.model

data class Card(
    val cardValue: CardValue,
    val cardSuit: CardSuit
) {
    override fun toString(): String {
        return "$cardValue$cardSuit"
    }
}