package com.example.pok.model

enum class CardSuit {
    Peaks, Clubs, Hearts, Diamonds
}