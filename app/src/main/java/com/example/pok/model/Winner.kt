package com.example.pok.model

data class Winner(
    val players: ArrayList<Player>,
    val combination: Combination
)