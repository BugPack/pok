package com.example.pok.model

enum class StepEnum {
    PREFLOP, FLOP, TURN, RIVER
}