package com.example.pok.model

enum class Combination(var comb: String) {
    HIGH_CART("high_cart"),
    PAIR("pair"),
    TWO_PAIRS("two_pairs"),
    TRIPLE("triple"),
    STREET("street"),
    FLASH("flash"),
    FULL_HOUSE("full_house"),
    CARE("care"),
    STREET_FLASH("street_flash"),
    FLASH_ROYAL("flash_royal")
}