package com.example.pok.model

data class HandCombination(
    val player: Player,
    val combination: Combination
)