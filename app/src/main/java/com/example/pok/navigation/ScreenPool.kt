package com.example.pok.navigation

import androidx.fragment.app.Fragment
import com.example.pok.ui.fragment.PokerFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object ScreenPool {
    class PokScreen() : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return PokerFragment.getInstance()
        }
    }
}