package com.example.pok

import android.os.CountDownTimer

object TimerUtils {
    fun getTimer(time: Long, tick: (Long) -> Unit, timeout: () -> Unit): CountDownTimer {
        return object : CountDownTimer(time, 1000) {
            override fun onFinish() {
                timeout()
            }

            override fun onTick(millisUntilFinished: Long) {
                tick(millisUntilFinished)
            }
        }
    }
}