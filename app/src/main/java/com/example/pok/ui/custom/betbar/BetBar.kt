package com.example.pok.ui.custom.betbar

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout

abstract class BetBar(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    abstract fun setInterface(betBarInterface: BetBarInterface)
}