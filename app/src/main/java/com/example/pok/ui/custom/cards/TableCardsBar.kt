package com.example.pok.ui.custom.cards

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.pok.R
import com.example.pok.model.Card
import com.example.pok.model.CardSuit
import com.pawegio.kandroid.visible
import kotlinx.android.synthetic.main.table_cards_bar_layout.view.*

class TableCardsBar(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    private var tableCardFirst: Card? = null
    private var tvTableCardFirst: TextView? = null
    private var ivTableCardFirst: ImageView? = null
    private var clTableCardFirst: ConstraintLayout? = null

    private var tableCardSecond: Card? = null
    private var tvTableCardSecond: TextView? = null
    private var ivTableCardSecond: ImageView? = null
    private var clTableCardSecond: ConstraintLayout? = null

    private var tableCardThird: Card? = null
    private var tvTableCardThird: TextView? = null
    private var ivTableCardThird: ImageView? = null
    private var clTableCardThird: ConstraintLayout? = null

    private var tableCardFour: Card? = null
    private var tvTableCardFour: TextView? = null
    private var ivTableCardFour: ImageView? = null
    private var clTableCardFour: ConstraintLayout? = null

    private var tableCardFive: Card? = null
    private var tvTableCardFive: TextView? = null
    private var ivTableCardFive: ImageView? = null
    private var clTableCardFive: ConstraintLayout? = null

    init {
        val inflater = LayoutInflater.from(context).inflate(R.layout.table_cards_bar_layout, this)
        tvTableCardFirst = inflater.findViewById(R.id.tvFirstCard)
        ivTableCardFirst = inflater.findViewById(R.id.ivFirstCard)
        clTableCardFirst = inflater.findViewById(R.id.clFirstCard)
        tvTableCardSecond = inflater.findViewById(R.id.tvSecondCard)
        ivTableCardSecond = inflater.findViewById(R.id.ivSecondCard)
        clTableCardSecond = inflater.findViewById(R.id.clSecondCard)
        tvTableCardThird = inflater.findViewById(R.id.tvThirdCard)
        ivTableCardThird = inflater.findViewById(R.id.ivThirdCard)
        clTableCardThird = inflater.findViewById(R.id.clThirdCard)
        tvTableCardFour = inflater.findViewById(R.id.tvFourCard)
        ivTableCardFour = inflater.findViewById(R.id.ivFourCard)
        clTableCardFour = inflater.findViewById(R.id.clFourCard)
        tvTableCardFive = inflater.findViewById(R.id.tvFiveCard)
        ivTableCardFive = inflater.findViewById(R.id.ivFiveCard)
        clTableCardFive = inflater.findViewById(R.id.clFiveCard)
    }

    fun setTableCards(cards: ArrayList<Card>) {
        when (cards.size) {
            3 -> {
                tableCardFirst = cards[0]
                tableCardSecond = cards[1]
                tableCardThird = cards[2]
            }
            4 -> {
                tableCardFirst = cards[0]
                tableCardSecond = cards[1]
                tableCardThird = cards[2]
                tableCardFour = cards[3]
            }
            5 -> {
                tableCardFirst = cards[0]
                tableCardSecond = cards[1]
                tableCardThird = cards[2]
                tableCardFour = cards[3]
                tableCardFive = cards[4]
            }
        }
        updateCards()
        checkCardsVisibility()
    }

    fun clearTableCards() {
        tableCardFirst = null
        tableCardSecond = null
        tableCardThird = null
        tableCardFour = null
        tableCardFive = null
        checkCardsVisibility()
        tvTableCardFirst?.text = ""
        ivTableCardFirst?.setImageDrawable(null)
        tvTableCardSecond?.text = ""
        ivTableCardSecond?.setImageDrawable(null)
        tvTableCardThird?.text = ""
        ivTableCardThird?.setImageDrawable(null)
        tvTableCardFour?.text = ""
        ivTableCardFour?.setImageDrawable(null)
        tvTableCardFive?.text = ""
        ivTableCardFive?.setImageDrawable(null)
    }

    private fun updateCards() {
        tableCardFirst?.let { card ->
            tvTableCardFirst?.text = card.cardValue.name.removePrefix("_")
            ivTableCardFirst?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
        tableCardSecond?.let { card ->
            tvTableCardSecond?.text = card.cardValue.name.removePrefix("_")
            ivTableCardSecond?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
        tableCardThird?.let { card ->
            tvTableCardThird?.text = card.cardValue.name.removePrefix("_")
            ivTableCardThird?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
        tableCardFour?.let { card ->
            tvTableCardFour?.text = card.cardValue.name.removePrefix("_")
            ivTableCardFour?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
        tableCardFive?.let { card ->
            tvTableCardFive?.text = card.cardValue.name.removePrefix("_")
            ivTableCardFive?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
    }

    private fun checkCardsVisibility() {
        tableCardFirst?.let {
            clFirstCard?.visible = true
        } ?: run {
            clFirstCard?.visible = false
        }
        tableCardSecond?.let {
            clSecondCard?.visible = true
        } ?: run {
            clSecondCard?.visible = false
        }
        tableCardThird?.let {
            clThirdCard?.visible = true
        } ?: run {
            clThirdCard?.visible = false
        }
        tableCardFour?.let {
            clFourCard?.visible = true
        } ?: run {
            clFourCard?.visible = false
        }
        tableCardFive?.let {
            clFiveCard?.visible = true
        } ?: run {
            clFiveCard?.visible = false
        }
    }

    private fun setCardSuit(imageView: ImageView, cardSuit: CardSuit) {
        when (cardSuit) {
            CardSuit.Peaks -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_peaky
                )
            )
            CardSuit.Clubs -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_club
                )
            )
            CardSuit.Hearts -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_heart
                )
            )
            CardSuit.Diamonds -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_diamond
                )
            )
        }
    }
}