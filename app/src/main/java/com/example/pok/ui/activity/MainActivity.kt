package com.example.pok.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pok.R
import com.example.pok.navigation.ScreenPool
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class MainActivity : AppCompatActivity(R.layout.activity_main) {
    private val router: Router by inject()
    private val navigatorHolder: NavigatorHolder by inject()

    private val navigator = object : SupportAppNavigator(this,
        R.id.container
    ) {
        override fun applyCommands(commands: Array<out Command>?) {
            super.applyCommands(commands)
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        router.newRootScreen(ScreenPool.PokScreen())
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }
}