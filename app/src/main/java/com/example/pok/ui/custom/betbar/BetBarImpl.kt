package com.example.pok.ui.custom.betbar

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ProgressBar
import android.widget.SeekBar
import android.widget.TextView
import com.example.pok.R
import com.example.pok.TimerUtils

class BetBarImpl(context: Context, attrs: AttributeSet) : BetBar(context, attrs) {

    private var maxCount = 5
    private var minCount = 0
    private var step = 0
    private var currentCount = 0
    private var myTimer: CountDownTimer? = null
    private val time = 10000L

    private var barInterface: BetBarInterface? = null
    private var progressBar: ProgressBar? = null
    private var seekBar: SeekBar? = null
    private var btnFold: Button? = null
    private var btnCall: Button? = null
    private var btnRaise: Button? = null
    private var tvCurrentCost: TextView? = null

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.BetBarImpl)
//        maxCount = typedArray.getInt(R.styleable.BetBar_max_count, 0)
//        bigBlind = typedArray.getInt(R.styleable.BetBar_big_blind, 0)
//        smallBlind = typedArray.getInt(R.styleable.BetBar_small_blind, 0)
        typedArray.recycle()

        val inflater = LayoutInflater.from(context).inflate(R.layout.bet_bar_layout, this)
        tvCurrentCost = inflater.findViewById(R.id.tvCurrentCost)

        progressBar = inflater.findViewById(R.id.progressBar)
        seekBar = inflater.findViewById(R.id.seekBar)

        btnFold = inflater.findViewById(R.id.btnFold)
        btnFold?.setOnClickListener { barInterface?.onClickFold() }

        btnCall = inflater.findViewById(R.id.btnCall)
        btnCall?.setOnClickListener { onClickCall() }

        btnRaise = inflater.findViewById(R.id.btnRaise)
        btnRaise?.setOnClickListener { onClickRaise() }
    }

    override fun setInterface(betBarInterface: BetBarInterface) {
        barInterface = betBarInterface
    }

    fun setMaxCount(cost: Int) {
        maxCount = cost
        seekBarUpdate()
    }

    fun setMinCount(cost: Int) {
        minCount = cost
        seekBarUpdate()
    }

    fun setStep(cost: Int) {
        step = cost
    }

    fun timerStart() {
        timerClear()
        myTimer = TimerUtils.getTimer(time, {
            val progress = (it * 100) / time
            setProgressBet(progress.toInt())
        }, {
            barInterface?.onClickFold()
            myTimer?.cancel()
            myTimer = null
        }).start()
    }

    private fun setProgressBet(progress: Int) {
        progressBar?.progress = progress
    }

    fun timerClear() {
        myTimer?.cancel()
        myTimer = null
    }

    private fun seekBarUpdate() {
        seekBar?.max = maxCount / step
        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                tvCurrentCost?.text = (progress * step).toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    private fun onClickCall() {
        barInterface?.onClickCall(currentCount)
    }

    private fun onClickRaise() {
        if (currentCount * 2 <= maxCount) {
            barInterface?.onClickCall(currentCount * 2)
        } else {
            barInterface?.onClickCall(maxCount)
        }
    }
}