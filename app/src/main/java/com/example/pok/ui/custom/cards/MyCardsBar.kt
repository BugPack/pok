package com.example.pok.ui.custom.cards

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.pok.R
import com.example.pok.model.Card
import com.example.pok.model.CardSuit

class MyCardsBar(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    private var myCardFirst: Card? = null
    private var tvMyCardFirst: TextView? = null
    private var ivMyCardFirst: ImageView? = null

    private var myCardSecond: Card? = null
    private var tvMyCardSecond: TextView? = null
    private var ivMyCardSecond: ImageView? = null

    init {
        val inflater = LayoutInflater.from(context).inflate(R.layout.my_cards_bar_layout, this)
        tvMyCardFirst = inflater.findViewById(R.id.tvFirstCard)
        ivMyCardFirst = inflater.findViewById(R.id.ivFirstCard)
        tvMyCardSecond = inflater.findViewById(R.id.tvSecondCard)
        ivMyCardSecond = inflater.findViewById(R.id.ivSecondCard)
    }

    fun setMyCards(cards: ArrayList<Card>) {
        if (cards.size == 2) {
            myCardFirst = cards.first()
            myCardSecond = cards[1]
        }
        updateCards()
    }

    fun clearMyCards() {
        myCardFirst = null
        myCardSecond = null
        tvMyCardFirst?.text = ""
        ivMyCardFirst?.setImageDrawable(null)
        tvMyCardSecond?.text = ""
        ivMyCardSecond?.setImageDrawable(null)
    }

    private fun updateCards() {
        myCardFirst?.let { card ->
            tvMyCardFirst?.text = card.cardValue.name.removePrefix("_")
            ivMyCardFirst?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
        myCardSecond?.let { card ->
            tvMyCardSecond?.text = card.cardValue.name.removePrefix("_")
            ivMyCardSecond?.let {
                setCardSuit(it, card.cardSuit)
            }
        }
    }

    private fun setCardSuit(imageView: ImageView, cardSuit: CardSuit) {
        when (cardSuit) {
            CardSuit.Peaks -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_peaky
                )
            )
            CardSuit.Clubs -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_club
                )
            )
            CardSuit.Hearts -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_heart
                )
            )
            CardSuit.Diamonds -> imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    context,
                    R.drawable.ic_diamond
                )
            )
        }
    }
}