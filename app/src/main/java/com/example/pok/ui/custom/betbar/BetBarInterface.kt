package com.example.pok.ui.custom.betbar

interface BetBarInterface {
    fun onClickFold()
    fun onClickCall(cost: Int)

}