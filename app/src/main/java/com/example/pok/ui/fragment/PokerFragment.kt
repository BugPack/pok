package com.example.pok.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.example.pok.CardUtils
import com.example.pok.R
import com.example.pok.model.Card
import com.example.pok.model.HandCombination
import com.example.pok.model.Player
import com.example.pok.model.StepEnum
import com.example.pok.ui.custom.betbar.BetBarInterface
import com.example.pok.ui.custom.cards.MyCardsBar
import kotlinx.android.synthetic.main.fragment_pok.*

class PokerFragment : Fragment(R.layout.fragment_pok) {
    companion object {
        fun getInstance() = PokerFragment()
    }

    val firstCards = arrayListOf<Card>()
    val secondCards = arrayListOf<Card>()
    val thirdCards = arrayListOf<Card>()
    val fourCards = arrayListOf<Card>()
    val fiveCards = arrayListOf<Card>()
    val myCards = arrayListOf<Card>()
    val tableCards = arrayListOf<Card>()
    var myMoney: Int = 100
    var smallBlind = 10
    var bigBlind = 20

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnRestart?.setOnClickListener {
            nextGame()
        }
    }

    override fun onResume() {
        super.onResume()
        startGame()
    }

    override fun onPause() {
        super.onPause()
        clearCards()
    }

    private fun startGame() {
        stepGame(StepEnum.PREFLOP)
    }

    private fun clearCards() {
        myCards.clear()
        tableCards.clear()
        firstCards.clear()
        secondCards.clear()
        thirdCards.clear()
        fourCards.clear()
        fiveCards.clear()
        setMyCards(null)
        clearTableCards()
        tvResult?.text = ""
        betBar?.timerClear()
    }

    private fun setMyCards(cards: ArrayList<Card>?) {
        cards?.let {
            myCardsBar?.setMyCards(it)
        } ?: run {
            myCardsBar?.clearMyCards()
        }
    }

    private fun setRivalCards(cardsBar: MyCardsBar, cards: ArrayList<Card>?) {
        cards?.let {
            cardsBar.setMyCards(it)
        } ?: run {
            cardsBar.clearMyCards()
        }
    }

    private fun setTableCards(cards: ArrayList<Card>) {
        tableCardsBar?.setTableCards(cards)
    }

    private fun clearTableCards() {
        tableCardsBar?.clearTableCards()
    }

    /**
     *  generation
     */
    private fun stepGame(stepEnum: StepEnum) {
        when (stepEnum) {
            StepEnum.PREFLOP -> {
                generateCards(myCards, 2)
                generateCards(firstCards, 2)
                generateCards(secondCards, 2)
                generateCards(thirdCards, 2)
                generateCards(fourCards, 2)
                generateCards(fiveCards, 2)
//                d("_log_cards", myCards.toString())
                setMyCards(myCards)
                //todo remove
                setRivalCards(firstCardsBar, firstCards)
                setRivalCards(secondCardsBar, secondCards)
                setRivalCards(thirdCardsBar, thirdCards)
                setRivalCards(fourCardsBar, fourCards)
                setRivalCards(fiveCardsBar, fiveCards)
                // todo end
                placeBet(bigBlind, smallBlind, myMoney, {
                    stepGame(StepEnum.FLOP)
                    stepGame(StepEnum.TURN)
                    stepGame(StepEnum.RIVER)
                    myCards.clear()
                    checkResult()
                    setMyCardsBarVisible(false)
                }, {
                    stepGame(StepEnum.FLOP)
                })
            }
            StepEnum.FLOP -> {
                generateCards(tableCards, 3)
//                d("_log_cards", tableCards.toString())
//                tvTableCards?.text = tableCards.toString()
                setTableCards(tableCards)

                placeBet(bigBlind, smallBlind, myMoney, {
                    stepGame(StepEnum.TURN)
                    stepGame(StepEnum.RIVER)
                    myCards.clear()
                    checkResult()
                    setMyCardsBarVisible(false)
                }, {
                    stepGame(StepEnum.TURN)
                })
            }
            StepEnum.TURN -> {
                generateCards(tableCards, 4)
//                d("_log_cards", tableCards.toString())
//                tvTableCards?.text = tableCards.toString()
                setTableCards(tableCards)

                placeBet(bigBlind, smallBlind, myMoney, {
                    stepGame(StepEnum.RIVER)
                    myCards.clear()
                    checkResult()
                    setMyCardsBarVisible(false)
                }, {
                    stepGame(StepEnum.RIVER)
                })
            }
            StepEnum.RIVER -> {
                generateCards(tableCards, 5)
//                d("_log_cards", tableCards.toString())
//                tvTableCards?.text = tableCards.toString()
                setTableCards(tableCards)

                placeBet(bigBlind, smallBlind, myMoney, {
                    myCards.clear()
                    checkResult()
                    setMyCardsBarVisible(false)
                }, {
                    checkResult()
                })
            }
        }
    }

    private fun generateCards(cardList: ArrayList<Card>, size: Int) {
        while (cardList.size < size) {
            val card = CardUtils.generateCard()
            if (!myCards.contains(card) && !tableCards.contains(card)
                && !firstCards.contains(card)
                && !secondCards.contains(card)
                && !thirdCards.contains(card)
                && !fourCards.contains(card)
                && !fiveCards.contains(card)
            ) {
                cardList.add(card)
            }
        }
    }

    private fun checkResult() {
        setPlaceBetVisible(false)
        betBar?.timerClear()
        val combMy = CardUtils.checkCombination(myCards, tableCards)
        val combFirst = CardUtils.checkCombination(firstCards, tableCards)
        val combSecond = CardUtils.checkCombination(secondCards, tableCards)
        val combThird = CardUtils.checkCombination(thirdCards, tableCards)
        val combFour = CardUtils.checkCombination(fourCards, tableCards)
        val combFive = CardUtils.checkCombination(fiveCards, tableCards)
        val combResult = CardUtils.compareCombination(
            arrayListOf(
                HandCombination(Player.MY, combMy),
                HandCombination(Player.FIRST, combFirst),
                HandCombination(Player.SECOND, combSecond),
                HandCombination(Player.THIRD, combThird),
                HandCombination(Player.FOUR, combFour),
                HandCombination(Player.FIVE, combFive)
            )
        )
        tvResult?.text =
            "my: ${combMy.comb}, first: ${combFirst.comb}, sec: ${combSecond.comb}, third: ${combThird.comb}, four: ${combFour.comb}, five: ${combFive.comb}, winner: ${combResult.players} ${combResult.combination}"
    }

    private fun nextGame() {
        clearCards()
        startGame()
    }

    private fun placeBet(
        step: Int,
        minCount: Int,
        maxCount: Int,
        onClickFold: () -> Unit,
        onClickCall: (Int) -> Unit
    ) {
        betBar?.setInterface(object : BetBarInterface {
            override fun onClickFold() {
                setPlaceBetVisible(false)
                onClickFold()
            }

            override fun onClickCall(cost: Int) {
                setPlaceBetVisible(false)
                onClickCall(cost)
            }
        })
        betBar?.timerStart()
        setPlaceBetVisible(true)
        setMyCardsBarVisible(true)
        betBar?.setStep(step)
        betBar?.setMaxCount(maxCount)
        betBar?.setMinCount(minCount)
    }

    private fun setPlaceBetVisible(isVisible: Boolean) {
        betBar?.visibility = if (isVisible) View.VISIBLE else View.GONE
        //todo remove
//        betBar?.visibility = View.VISIBLE
    }

    private fun setMyCardsBarVisible(isVisible: Boolean) {
        myCardsBar?.visibility = if (isVisible) View.VISIBLE else View.GONE
    }
}