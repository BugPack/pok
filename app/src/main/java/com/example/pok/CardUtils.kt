package com.example.pok

import android.util.Log
import com.example.pok.model.*

object CardUtils {

    fun generateCard() = Card(
        cardValue = generateValue(),
        cardSuit = generateSuit()
    )

    fun generateValue(): CardValue {
        return when (random(12)) {
            0 -> CardValue._2
            1 -> CardValue._3
            2 -> CardValue._4
            3 -> CardValue._5
            4 -> CardValue._6
            5 -> CardValue._7
            6 -> CardValue._8
            7 -> CardValue._9
            8 -> CardValue._10
            9 -> CardValue._J
            10 -> CardValue._Q
            11 -> CardValue._K
            12 -> CardValue._A
            else -> TODO()
        }
    }

    fun generateSuit(): CardSuit {
        return when (random(3)) {
            0 -> CardSuit.Peaks
            1 -> CardSuit.Clubs
            2 -> CardSuit.Hearts
            3 -> CardSuit.Diamonds
            else -> TODO()
        }
    }

    fun random(range: Int) = (0..range).random()

    /**
     *  check
     */

    fun selector(card: Card): CardValue = card.cardValue

    fun checkCombination(playerCards: ArrayList<Card>, tableCards: ArrayList<Card>): Combination {
        val cards = ArrayList<Card>()
        cards.addAll(playerCards)
        cards.addAll(tableCards)

        cards.sortBy { selector(it) }

        Log.d("_log_cards", cards.toString())
        return when {
            checkFlashRoyal(cards) -> {
                Log.d("_log_cards", "FlashRoyal")
                Combination.FLASH_ROYAL
            }
            checkStreetFlash(cards) -> {
                Log.d("_log_cards", "StreetFlash")
                Combination.STREET_FLASH
            }
            checkCare(cards) -> {
                Log.d("_log_cards", "Care")
                Combination.CARE
            }
            checkFullHouse(cards) -> {
                Log.d("_log_cards", "FullHouse")
                Combination.FULL_HOUSE
            }
            checkFlash(cards) -> {
                Log.d("_log_cards", "Flash")
                Combination.FLASH
            }
            checkStreet(cards) -> {
                Log.d("_log_cards", "Street")
                Combination.STREET
            }
            checkTriple(cards) -> {
                Log.d("_log_cards", "Triple")
                Combination.TRIPLE
            }
            checkTwoPairs(cards) -> {
                Log.d("_log_cards", "TwoPairs")
                Combination.TWO_PAIRS
            }
            checkPair(cards) -> {
                Log.d("_log_cards", "Pair")
                Combination.PAIR
            }
            else -> {
                val highCard = findHighCard(cards)
                Log.d("_log_cards", "HighCard: $highCard")
                Combination.HIGH_CART
            }
        }
    }

    /**
     * combinations:
     * FlashRoyal
     * StreetFlash
     * Care
     * FullHouse
     * Flash
     * Street
     * Triple
     * TwoPairs
     * Pairs
     * HighCard
     */

    fun checkFlashRoyal(cards: ArrayList<Card>): Boolean {
        return if (checkFlash(cards)) {
            val cardsOfSuit = filterSuit(cards, findCurrentSuit(cards))
            val highCard = findHighCard(cardsOfSuit).cardValue
            checkStreet(cardsOfSuit) && highCard == CardValue._A
        } else {
            false
        }
    }

    fun checkStreetFlash(cards: ArrayList<Card>): Boolean {
        return if (checkFlash(cards)) {
            val cardsOfSuit = filterSuit(cards, findCurrentSuit(cards))
            checkStreet(cardsOfSuit)
        } else {
            false
        }
    }

    fun checkCare(cards: ArrayList<Card>): Boolean {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        calculateValues(cards) { _2, _3, _4, _5, _6, _7, _8, _9, _10, _J, _Q, _K, _A ->
            count2 = _2
            count3 = _3
            count4 = _4
            count5 = _5
            count6 = _6
            count7 = _7
            count8 = _8
            count9 = _9
            count10 = _10
            countJ = _J
            countQ = _Q
            countK = _K
            countA = _A
        }
        return count2 == 4 || count3 == 4 || count4 == 4 || count5 == 4 || count6 == 4 ||
                count7 == 4 || count8 == 4 || count9 == 4 || count10 == 4 || countJ == 4 ||
                countQ == 4 || countK == 4 || countA == 4
    }

    fun checkFullHouse(cards: ArrayList<Card>): Boolean {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        calculateValues(cards) { _2, _3, _4, _5, _6, _7, _8, _9, _10, _J, _Q, _K, _A ->
            count2 = _2
            count3 = _3
            count4 = _4
            count5 = _5
            count6 = _6
            count7 = _7
            count8 = _8
            count9 = _9
            count10 = _10
            countJ = _J
            countQ = _Q
            countK = _K
            countA = _A
        }
        // todo check this
        return count2 == 2 && count3 == 3 || count2 == 2 && count4 == 3 || count2 == 2 && count5 == 3 ||
                count2 == 2 && count6 == 3 || count2 == 2 && count7 == 3 || count2 == 2 && count8 == 3 ||
                count2 == 2 && count9 == 3 || count2 == 2 && count10 == 3 || count2 == 2 && countJ == 3 ||
                count2 == 2 && countQ == 3 || count2 == 2 && countK == 3 || count2 == 2 && countA == 3 ||

                count2 == 3 && count3 == 2 || count2 == 3 && count4 == 2 || count2 == 3 && count5 == 2 ||
                count2 == 3 && count6 == 2 || count2 == 3 && count7 == 2 || count2 == 3 && count8 == 2 ||
                count2 == 3 && count9 == 2 || count2 == 3 && count10 == 2 || count2 == 3 && countJ == 2 ||
                count2 == 3 && countQ == 2 || count2 == 3 && countK == 2 || count2 == 3 && countA == 2 ||


                count3 == 2 && count4 == 3 || count3 == 2 && count5 == 3 ||
                count3 == 2 && count6 == 3 || count3 == 2 && count7 == 3 || count3 == 2 && count8 == 3 ||
                count3 == 2 && count9 == 3 || count3 == 2 && count10 == 3 || count3 == 2 && countJ == 3 ||
                count3 == 2 && countQ == 3 || count3 == 2 && countK == 3 || count3 == 2 && countA == 3 ||

                count3 == 3 && count4 == 2 || count3 == 3 && count5 == 2 ||
                count3 == 3 && count6 == 2 || count3 == 3 && count7 == 2 || count3 == 3 && count8 == 2 ||
                count3 == 3 && count9 == 2 || count3 == 3 && count10 == 2 || count3 == 3 && countJ == 2 ||
                count3 == 3 && countQ == 2 || count3 == 3 && countK == 2 || count3 == 3 && countA == 2 ||


                count4 == 2 && count5 == 3 ||
                count4 == 2 && count6 == 3 || count4 == 2 && count7 == 3 || count4 == 2 && count8 == 3 ||
                count4 == 2 && count9 == 3 || count4 == 2 && count10 == 3 || count4 == 2 && countJ == 3 ||
                count4 == 2 && countQ == 3 || count4 == 2 && countK == 3 || count4 == 2 && countA == 3 ||

                count4 == 3 && count5 == 2 ||
                count4 == 3 && count6 == 2 || count4 == 3 && count7 == 2 || count4 == 3 && count8 == 2 ||
                count4 == 3 && count9 == 2 || count4 == 3 && count10 == 2 || count4 == 3 && countJ == 2 ||
                count4 == 3 && countQ == 2 || count4 == 3 && countK == 2 || count4 == 3 && countA == 2 ||


                count5 == 2 && count6 == 3 || count5 == 2 && count7 == 3 || count5 == 2 && count8 == 3 ||
                count5 == 2 && count9 == 3 || count5 == 2 && count10 == 3 || count5 == 2 && countJ == 3 ||
                count5 == 2 && countQ == 3 || count5 == 2 && countK == 3 || count5 == 2 && countA == 3 ||

                count5 == 3 && count6 == 2 || count5 == 3 && count7 == 2 || count5 == 3 && count8 == 2 ||
                count5 == 3 && count9 == 2 || count5 == 3 && count10 == 2 || count5 == 3 && countJ == 2 ||
                count5 == 3 && countQ == 2 || count5 == 3 && countK == 2 || count5 == 3 && countA == 2 ||


                count6 == 2 && count7 == 3 || count6 == 2 && count8 == 3 ||
                count6 == 2 && count9 == 3 || count6 == 2 && count10 == 3 || count6 == 2 && countJ == 3 ||
                count6 == 2 && countQ == 3 || count6 == 2 && countK == 3 || count6 == 2 && countA == 3 ||

                count6 == 3 && count7 == 2 || count6 == 3 && count8 == 2 ||
                count6 == 3 && count9 == 2 || count6 == 3 && count10 == 2 || count6 == 3 && countJ == 2 ||
                count6 == 3 && countQ == 2 || count6 == 3 && countK == 2 || count6 == 3 && countA == 2 ||


                count7 == 2 && count8 == 3 ||
                count7 == 2 && count9 == 3 || count7 == 2 && count10 == 3 || count7 == 2 && countJ == 3 ||
                count7 == 2 && countQ == 3 || count7 == 2 && countK == 3 || count7 == 2 && countA == 3 ||

                count7 == 3 && count8 == 2 ||
                count7 == 3 && count9 == 2 || count7 == 3 && count10 == 2 || count7 == 3 && countJ == 2 ||
                count7 == 3 && countQ == 2 || count7 == 3 && countK == 2 || count7 == 3 && countA == 2 ||


                count8 == 2 && count9 == 3 || count8 == 2 && count10 == 3 || count8 == 2 && countJ == 3 ||
                count8 == 2 && countQ == 3 || count8 == 2 && countK == 3 || count8 == 2 && countA == 3 ||

                count8 == 3 && count9 == 2 || count8 == 3 && count10 == 2 || count8 == 3 && countJ == 2 ||
                count8 == 3 && countQ == 2 || count8 == 3 && countK == 2 || count8 == 3 && countA == 2 ||


                count9 == 2 && count10 == 3 || count9 == 2 && countJ == 3 ||
                count9 == 2 && countQ == 3 || count9 == 2 && countK == 3 || count9 == 2 && countA == 3 ||

                count9 == 3 && count10 == 2 || count9 == 3 && countJ == 2 ||
                count9 == 3 && countQ == 2 || count9 == 3 && countK == 2 || count9 == 3 && countA == 2 ||


                count10 == 2 && countJ == 3 ||
                count10 == 2 && countQ == 3 || count10 == 2 && countK == 3 || count10 == 2 && countA == 3 ||

                count10 == 3 && countJ == 2 ||
                count10 == 3 && countQ == 2 || count10 == 3 && countK == 2 || count10 == 3 && countA == 2 ||


                countJ == 2 && countQ == 3 || countJ == 2 && countK == 3 || countJ == 2 && countA == 3 ||

                countJ == 3 && countQ == 2 || countJ == 3 && countK == 2 || countJ == 3 && countA == 2 ||


                countQ == 2 && countK == 3 || countQ == 2 && countA == 3 ||

                countQ == 3 && countK == 2 || countQ == 3 && countA == 2 ||


                countK == 2 && countA == 3 ||

                countK == 3 && countA == 2
    }

    fun checkFlash(cards: ArrayList<Card>): Boolean {
        var counterPeaks = 0
        var counterClubs = 0
        var counterHearts = 0
        var counterDiamonds = 0
        calculateSuit(cards) { peaks, clubs, hearts, diamonds ->
            counterPeaks = peaks
            counterClubs = clubs
            counterHearts = hearts
            counterDiamonds = diamonds
        }
        return counterPeaks >= 5 || counterClubs >= 5 || counterHearts >= 5 || counterDiamonds >= 5
    }

    fun checkStreet(cards: ArrayList<Card>): Boolean {
        var count = 1
        for (i in 0 until cards.size - 1) {
            if (cards[i].cardValue == CardValue._A && cards[i + 1].cardValue == CardValue._2 ||
                cards[i].cardValue == CardValue._2 && cards[i + 1].cardValue == CardValue._3 ||
                cards[i].cardValue == CardValue._3 && cards[i + 1].cardValue == CardValue._4 ||
                cards[i].cardValue == CardValue._4 && cards[i + 1].cardValue == CardValue._5 ||
                cards[i].cardValue == CardValue._5 && cards[i + 1].cardValue == CardValue._6 ||
                cards[i].cardValue == CardValue._6 && cards[i + 1].cardValue == CardValue._7 ||
                cards[i].cardValue == CardValue._7 && cards[i + 1].cardValue == CardValue._8 ||
                cards[i].cardValue == CardValue._8 && cards[i + 1].cardValue == CardValue._9 ||
                cards[i].cardValue == CardValue._9 && cards[i + 1].cardValue == CardValue._10 ||
                cards[i].cardValue == CardValue._10 && cards[i + 1].cardValue == CardValue._J ||
                cards[i].cardValue == CardValue._J && cards[i + 1].cardValue == CardValue._Q ||
                cards[i].cardValue == CardValue._Q && cards[i + 1].cardValue == CardValue._K ||
                cards[i].cardValue == CardValue._K && cards[i + 1].cardValue == CardValue._A
            ) {
                count++
            }
        }
        return count >= 5
    }

    fun checkTriple(cards: ArrayList<Card>): Boolean {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        calculateValues(cards) { _2, _3, _4, _5, _6, _7, _8, _9, _10, _J, _Q, _K, _A ->
            count2 = _2
            count3 = _3
            count4 = _4
            count5 = _5
            count6 = _6
            count7 = _7
            count8 = _8
            count9 = _9
            count10 = _10
            countJ = _J
            countQ = _Q
            countK = _K
            countA = _A
        }
        return count2 == 3 || count3 == 3 || count4 == 3 || count5 == 3 || count6 == 3 ||
                count7 == 3 || count8 == 3 || count9 == 3 || count10 == 3 || countJ == 3 ||
                countQ == 3 || countK == 3 || countA == 3
    }

    fun checkTwoPairs(cards: ArrayList<Card>): Boolean {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        calculateValues(cards) { _2, _3, _4, _5, _6, _7, _8, _9, _10, _J, _Q, _K, _A ->
            count2 = _2
            count3 = _3
            count4 = _4
            count5 = _5
            count6 = _6
            count7 = _7
            count8 = _8
            count9 = _9
            count10 = _10
            countJ = _J
            countQ = _Q
            countK = _K
            countA = _A
        }
        return count2 == 2 && count3 == 2 || count2 == 2 && count4 == 2 || count2 == 2 && count5 == 2 ||
                count2 == 2 && count6 == 2 || count2 == 2 && count7 == 2 || count2 == 2 && count8 == 2 ||
                count2 == 2 && count9 == 2 || count2 == 2 && count10 == 2 || count2 == 2 && countJ == 2 ||
                count2 == 2 && countQ == 2 || count2 == 2 && countK == 2 || count2 == 2 && countA == 2 ||

                count3 == 2 && count4 == 2 || count3 == 2 && count5 == 2 ||
                count3 == 2 && count6 == 2 || count3 == 2 && count7 == 2 || count3 == 2 && count8 == 2 ||
                count3 == 2 && count9 == 2 || count3 == 2 && count10 == 2 || count3 == 2 && countJ == 2 ||
                count3 == 2 && countQ == 2 || count3 == 2 && countK == 2 || count3 == 2 && countA == 2 ||

                count4 == 2 && count5 == 2 ||
                count4 == 2 && count6 == 2 || count4 == 2 && count7 == 2 || count4 == 2 && count8 == 2 ||
                count4 == 2 && count9 == 2 || count4 == 2 && count10 == 2 || count4 == 2 && countJ == 2 ||
                count4 == 2 && countQ == 2 || count4 == 2 && countK == 2 || count4 == 2 && countA == 2 ||

                count5 == 2 && count6 == 2 || count5 == 2 && count7 == 2 || count5 == 2 && count8 == 2 ||
                count5 == 2 && count9 == 2 || count5 == 2 && count10 == 2 || count5 == 2 && countJ == 2 ||
                count5 == 2 && countQ == 2 || count5 == 2 && countK == 2 || count5 == 2 && countA == 2 ||

                count6 == 2 && count7 == 2 || count6 == 2 && count8 == 2 ||
                count6 == 2 && count9 == 2 || count6 == 2 && count10 == 2 || count6 == 2 && countJ == 2 ||
                count6 == 2 && countQ == 2 || count6 == 2 && countK == 2 || count6 == 2 && countA == 2 ||

                count6 == 2 && count7 == 2 || count6 == 2 && count8 == 2 ||
                count6 == 2 && count9 == 2 || count6 == 2 && count10 == 2 || count6 == 2 && countJ == 2 ||
                count6 == 2 && countQ == 2 || count6 == 2 && countK == 2 || count6 == 2 && countA == 2 ||

                count7 == 2 && count8 == 2 ||
                count7 == 2 && count9 == 2 || count7 == 2 && count10 == 2 || count7 == 2 && countJ == 2 ||
                count7 == 2 && countQ == 2 || count7 == 2 && countK == 2 || count7 == 2 && countA == 2 ||

                count8 == 2 && count9 == 2 || count8 == 2 && count10 == 2 || count8 == 2 && countJ == 2 ||
                count8 == 2 && countQ == 2 || count8 == 2 && countK == 2 || count8 == 2 && countA == 2 ||

                count9 == 2 && count10 == 2 || count9 == 2 && countJ == 2 ||
                count9 == 2 && countQ == 2 || count9 == 2 && countK == 2 || count9 == 2 && countA == 2 ||

                count10 == 2 && countJ == 2 ||
                count10 == 2 && countQ == 2 || count10 == 2 && countK == 2 || count10 == 2 && countA == 2 ||

                countJ == 2 && countQ == 2 || countJ == 2 && countK == 2 || countJ == 2 && countA == 2 ||

                countQ == 2 && countK == 2 || countQ == 2 && countA == 2 ||

                countK == 2 && countA == 2
    }

    fun checkPair(cards: ArrayList<Card>): Boolean {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        calculateValues(cards) { _2, _3, _4, _5, _6, _7, _8, _9, _10, _J, _Q, _K, _A ->
            count2 = _2
            count3 = _3
            count4 = _4
            count5 = _5
            count6 = _6
            count7 = _7
            count8 = _8
            count9 = _9
            count10 = _10
            countJ = _J
            countQ = _Q
            countK = _K
            countA = _A
        }
        return count2 == 2 || count3 == 2 || count4 == 2 || count5 == 2 || count6 == 2 ||
                count7 == 2 || count8 == 2 || count9 == 2 || count10 == 2 || countJ == 2 ||
                countQ == 2 || countK == 2 || countA == 2
    }

    fun findHighCard(cards: ArrayList<Card>): Card {
        var highCard = Card(
            cardSuit = CardSuit.Diamonds,
            cardValue = CardValue._2
        )
        cards.forEach {
            if (it.cardValue.position > highCard.cardValue.position) {
                highCard = it
            }
        }
        return highCard
    }

    fun calculateValues(
        cards: ArrayList<Card>,
        result: (
            _2: Int, _3: Int, _4: Int, _5: Int, _6: Int, _7: Int, _8: Int, _9: Int, _10: Int,
            _J: Int, _Q: Int, _K: Int, _A: Int
        ) -> Unit
    ) {
        var count2 = 0
        var count3 = 0
        var count4 = 0
        var count5 = 0
        var count6 = 0
        var count7 = 0
        var count8 = 0
        var count9 = 0
        var count10 = 0
        var countJ = 0
        var countQ = 0
        var countK = 0
        var countA = 0
        cards.forEach {
            when (it.cardValue) {
                CardValue._2 -> count2++
                CardValue._3 -> count3++
                CardValue._4 -> count4++
                CardValue._5 -> count5++
                CardValue._6 -> count6++
                CardValue._7 -> count7++
                CardValue._8 -> count8++
                CardValue._9 -> count9++
                CardValue._10 -> count10++
                CardValue._J -> countJ++
                CardValue._Q -> countQ++
                CardValue._K -> countK++
                CardValue._A -> countA++
            }
        }
//        d(
//            "_log_cards",
//            "_2: $count2, _3: $count3, _4: $count4, _5: $count5, _6: $count6, _7: $count7, _8: $count8, _9: $count9, _10: $count10, _J: $countJ, _Q: $countQ, _K: $countK, _A: $countA"
//        )
        result(
            count2, count3, count4, count5, count6, count7, count8, count9, count10, countJ, countQ,
            countK, countA
        )
    }

    fun calculateSuit(
        cards: ArrayList<Card>,
        result: (peaks: Int, clubs: Int, hearts: Int, diamonds: Int) -> Unit
    ) {
        var counterPeaks = 0
        var counterClubs = 0
        var counterHearts = 0
        var counterDiamonds = 0
        cards.forEach {
            when (it.cardSuit) {
                CardSuit.Peaks -> counterPeaks++
                CardSuit.Clubs -> counterClubs++
                CardSuit.Hearts -> counterHearts++
                CardSuit.Diamonds -> counterDiamonds++
            }
        }
//        d(
//            "_log_cards",
//            "peaks: $counterPeaks, clubs: $counterClubs, hearts: $counterHearts, diamonds: $counterDiamonds"
//        )
        result(counterPeaks, counterClubs, counterHearts, counterDiamonds)
    }

    fun findCurrentSuit(cards: ArrayList<Card>): CardSuit {
        var counterPeaks = 0
        var counterClubs = 0
        var counterHearts = 0
        var counterDiamonds = 0
        cards.forEach {
            when (it.cardSuit) {
                CardSuit.Peaks -> counterPeaks++
                CardSuit.Clubs -> counterClubs++
                CardSuit.Hearts -> counterHearts++
                CardSuit.Diamonds -> counterDiamonds++
            }
        }
        return if (counterPeaks > counterClubs && counterPeaks > counterHearts && counterPeaks > counterDiamonds) {
            CardSuit.Peaks
        } else if (counterClubs > counterPeaks && counterClubs > counterHearts && counterClubs > counterDiamonds) {
            CardSuit.Clubs
        } else if (counterHearts > counterPeaks && counterHearts > counterClubs && counterHearts > counterDiamonds) {
            CardSuit.Hearts
        } else {
            CardSuit.Diamonds
        }
    }

    fun filterSuit(cards: ArrayList<Card>, cardSuit: CardSuit): ArrayList<Card> {
        val suitCards = arrayListOf<Card>()
        cards.forEach {
            if (it.cardSuit == cardSuit) {
                suitCards.add(it)
            }
        }
        return suitCards
    }

    /**
     * Compare combinations
     */
    fun compareCombination(handCombinations: ArrayList<HandCombination>): Winner {
        var high_cart = 0
        var pair = 0
        var two_pairs = 0
        var triple = 0
        var street = 0
        var flash = 0
        var full_house = 0
        var care = 0
        var street_flash = 0
        var flash_royal = 0
        handCombinations.forEach {
            when (it.combination) {
                Combination.HIGH_CART -> high_cart++
                Combination.PAIR -> pair++
                Combination.TWO_PAIRS -> two_pairs++
                Combination.TRIPLE -> triple++
                Combination.STREET -> street++
                Combination.FLASH -> flash++
                Combination.FULL_HOUSE -> full_house++
                Combination.CARE -> care++
                Combination.STREET_FLASH -> street_flash++
                Combination.FLASH_ROYAL -> flash_royal++
            }
        }
        //todo добавить сравнение двух или более одинаковых комбинаций
        if (flash_royal > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.FLASH_ROYAL) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.FLASH_ROYAL)
        }
        if (street_flash > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.STREET_FLASH) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.STREET_FLASH)
        }
        if (care > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.CARE) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.CARE)
        }
        if (full_house > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.FULL_HOUSE) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.FULL_HOUSE)
        }
        if (flash > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.FLASH) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.FLASH)
        }
        if (street > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.STREET) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.STREET)
        }
        if (triple > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.TRIPLE) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.TRIPLE)
        }
        if (two_pairs > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.TWO_PAIRS) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.TWO_PAIRS)
        }
        if (pair > 0) {
            val players = arrayListOf<Player>()
            handCombinations.forEach {
                if (it.combination == Combination.PAIR) {
                    players.add(it.player)
                }
            }
            return Winner(players, Combination.PAIR)
        }
        val players = arrayListOf<Player>()
        handCombinations.forEach {
            if (it.combination == Combination.HIGH_CART) {
                players.add(it.player)
            }
        }
        return Winner(players, Combination.HIGH_CART)
    }
}